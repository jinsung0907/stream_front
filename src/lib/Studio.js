import { apifetch } from "./Fetch";

export async function getMyVideoList() {
  return await apifetch({ url: "/studio/mylist", method: "POST" });
}

export async function getVideoEncodeStatus(videoId) {
  return await apifetch({
    url: "/studio/videoEncodeStatus",
    method: "POST",
    data: {
      videoId: videoId,
    },
  });
}
