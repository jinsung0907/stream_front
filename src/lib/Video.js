import { apifetch } from "./Fetch";

export async function getVideoInfoById(id) {
  return await apifetch({ url: "/video/" + id, method: "get" });
}

export async function getVideoSubtitles(id) {
  return await apifetch({ url: `/video/${id}/subtitle`, method: "get" });
}

export async function getVideoComments(id) {
  return await apifetch({ url: `/video/${id}/comments`, method: "get" });
}

export async function videoLike(id) {
  return await apifetch({
    url: `/video/like`,
    method: "post",
    data: {
      videoId: id,
      type: true,
    },
  });
}

export async function videoDislike(id) {
  return await apifetch({
    url: `/video/like`,
    method: "post",
    data: {
      videoId: id,
      type: false,
    },
  });
}

export async function videoViewCount(id) {
  return await apifetch({ url: `/video/${id}/view`, method: "get" });
}

export async function getComments(videoId, paging) {
  return await apifetch({
    url: `/video/${videoId}/comments${paging && "?paging=" + paging}`,
    method: "get",
  });
}

export async function postComment({ videoId, parentId, comment }) {
  return await apifetch({
    url: `/video/comment`,
    method: "post",
    data: {
      videoId: videoId,
      parentId: parentId,
      comment: comment,
    },
  });
}

export async function deleteComment({ commentId }) {
  return await apifetch({
    url: `/video/comment/${commentId}`,
    method: "delete",
  });
}

export async function modifyVideoInfo(info) {
  return await apifetch({
    url: `/video/edit`,
    method: "post",
    data: info,
  });
}

export async function getVideoList(paging) {
  let url = "/video/list";
  if (paging) {
    url += "/" + paging;
  }
  return await apifetch({ url: url });
}

export function lengthToDisplay(length) {
  let seconds = length % 60;
  let minutes = Math.floor(length / 60) % 60;
  let hours = Math.floor(length / 3600);

  let result = [];
  if (hours > 0) result.push(hours);
  result.push(minutes);
  result.push(seconds.toString().padStart(2, "0"));

  return result.join(":");
}

export function visibleToString(visible) {
  switch (visible) {
    case "public":
      return "공개";
    case "unlisted":
      return "미공개";
    case "private":
      return "비공개";
    default:
      return "err";
  }
}
