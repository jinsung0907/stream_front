import { apifetch } from "./Fetch";

export async function doRegister({ email, password, name }) {
  return await apifetch({
    url: "/register",
    method: "post",
    data: {
      email: email,
      password: password,
      name: name,
    },
  });
}

export async function doLogin(email, password) {
  return await apifetch({
    url: "/login",
    method: "post",
    data: {
      email: email,
      password: password,
    },
  });
}

export async function doLogout() {
  return await apifetch({ url: "/logout", method: "get" });
}

export async function getUserMyInfo() {
  return await apifetch({ url: "/user/myinfo", method: "get" });
}
