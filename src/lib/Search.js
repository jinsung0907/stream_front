import { apifetch } from "./Fetch";

export async function doSearch(searchstr) {
  return await apifetch({
    url: `/search`,
    method: "post",
    data: { searchstr: searchstr },
  });
}
