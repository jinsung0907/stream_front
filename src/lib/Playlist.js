import { apifetch } from "./Fetch";

export async function getUserPlaylists(videoId) {
  let params = "";
  if (typeof videoId !== "undefined") {
    params = new URLSearchParams({ videoId: videoId }).toString();
  }
  return await apifetch({
    url: `/playlist/mylists${params !== "" && "?" + params}`,
    method: "get",
  });
}

export async function createPlaylist(data) {
  return await apifetch({
    url: `/playlist/create`,
    method: "post",
    data: data,
  });
}

export async function addPlayListItem(playlistId, videoId) {
  return await apifetch({
    url: `/playlist/item/add`,
    method: "post",
    data: {
      playlistId: playlistId,
      videoId: videoId,
    },
  });
}

export async function deletePlayListItem(itemId) {
  return await apifetch({
    url: `/playlist/item/${itemId}`,
    method: "delete",
  });
}

export async function getPlaylistById(itemId) {
  return await apifetch({
    url: `/playlist/${itemId}`,
    method: "get",
  });
}

export async function modifyPlaylist(playlistId, info) {
  return await apifetch({
    url: `/playlist/edit`,
    method: "post",
    data: {
      playlistId: playlistId,
      info: info,
    },
  });
}

export async function deletePlaylist(playlistId) {
  return await apifetch({
    url: `/playlist`,
    method: "delete",
    data: {
      playlistId: playlistId,
    },
  });
}

export async function getPlaylistList(paging) {
  let url = "/playlist/list";
  if (paging) {
    url += "/" + paging;
  }
  return await apifetch({ url: url });
}

export async function reOrderPlaylist(playlistId, itemId, to) {
  return await apifetch({
    url: "/playlist/item/edit",
    method: "post",
    data: {
      playlistId: playlistId,
      itemId: itemId,
      sequence: to,
    },
  });
}
