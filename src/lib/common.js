export function formdataToObject(formdata) {
  let object = {};
  formdata.forEach((value, key) => (object[key] = value));
  return object;
}

export function handleFetchErrorWithToast(res, toast) {
  if (res.errorMsg) {
    toast({
      title: "에러발생",
      status: "error",
      description: res.errorMsg,
      ducation: 2500,
    });
  } else {
    toast({
      title: "GENERIC ERROR",
      status: "error",
      description: res.errorMsg,
      ducation: 2500,
    });
  }
}
