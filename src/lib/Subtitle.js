import { apifetch } from "./Fetch";

export async function subtitleLike(id) {
  return await apifetch({
    url: `/subtitle/vote`,
    method: "post",
    data: {
      subtitleId: id,
      type: true,
    },
  });
}

export async function subtitleDislike(id) {
  return await apifetch({
    url: `/subtitle/vote`,
    method: "post",
    data: {
      subtitleId: id,
      type: false,
    },
  });
}
