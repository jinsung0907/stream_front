import {
  ButtonGroup,
  IconButton,
  Box,
  Flex,
  Link,
  Text,
} from "@chakra-ui/react";
import dayjs from "dayjs";
import { FiCornerDownRight } from "react-icons/fi";
const VideoComment = ({ info }) => {
  /*
  if (typeof info === "undefined") {
    return null;
  }
  */

  return (
    <Box
      w="100%"
      borderRadius="5px"
      px="2"
      backgroundColor={info.highlight && "lightcyan"}
    >
      <Flex flexFlow="row" alignItems="center">
        <Link pr="4" fontWeight="bold" fontSize="md">
          {info.userId}
        </Link>{" "}
        <Text color="gray.500" fontSize="xs">
          {dayjs.unix(info.insertDate).format("YYYY. MM. DD  HH:mm")}
        </Text>
      </Flex>
      <Text>{info.comment}</Text>
      <Box>
        <ButtonGroup isAttached>
          <IconButton size="sm" variant="ghost" icon={<FiCornerDownRight />} />
          <IconButton size="sm" variant="ghost" icon={<FiCornerDownRight />} />
          <IconButton size="sm" variant="ghost" icon={<FiCornerDownRight />} />
          <IconButton size="sm" variant="ghost" icon={<FiCornerDownRight />} />
        </ButtonGroup>
      </Box>
    </Box>
  );
};

export default VideoComment;
