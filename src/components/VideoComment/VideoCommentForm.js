import {
  Button,
  FormErrorMessage,
  FormControl,
  useDisclosure,
  Textarea,
  Collapse,
} from "@chakra-ui/react";
import { useState } from "react";
import TextareaAutosize from "react-textarea-autosize";
import { useUserState } from "../../hooks/UserContext";
import { postComment } from "../../lib/Video";

const VideoCommentForm = ({ videoInfo, isReadOnly, onSubmit }) => {
  const userInfo = useUserState();
  const [errorMsg, setErrorMsg] = useState();
  const [TextareaValue, setTextareaValue] = useState("");
  const [isPosting, setPosting] = useState(false);
  const { isOpen, onOpen, onClose } = useDisclosure();

  const handleChange = (e) => setTextareaValue(e.target.value);

  const onCommentSubmit = async (e) => {
    e.preventDefault();

    if (TextareaValue.length === 0) {
      setErrorMsg("댓글을 입력해주세요.");
      return;
    }

    try {
      setPosting(true);
      let res = await postComment({
        videoId: videoInfo.id,
        comment: TextareaValue,
      });
      setPosting(false);

      if (res.status === "success") {
        //TODO
        //댓글 목록 새로고침 한 다음 맨 위에 작성한 댓글 넣기
        onSubmit(TextareaValue);
        setTextareaValue("");
        onClose();
      } else if (res.errorMsg) {
        setErrorMsg(res.errorMsg);
      } else {
        setErrorMsg("에러 발생");
      }
    } catch (e) {
      console.error(e);
      setPosting(false);
      setErrorMsg("에러 발생");
    }
  };

  return (
    <form>
      <FormControl isReadOnly={isReadOnly} isInvalid={errorMsg} mb="4">
        <Textarea
          id="postCommentMain"
          as={TextareaAutosize}
          variant="flushed"
          borderColor="blackAlpha.500"
          borderBottom="2px"
          placeholder={
            userInfo.logined
              ? "댓글 작성..."
              : "댓글 작성을 위해 로그인이 필요합니다."
          }
          disabled={!userInfo.logined}
          rows="1"
          overflowY="hidden"
          resize="none"
          onFocus={() => {
            setErrorMsg();
            onOpen();
          }}
          onBlur={onClose}
          value={TextareaValue}
          onChange={handleChange}
        />
        <FormErrorMessage>{errorMsg}</FormErrorMessage>
      </FormControl>
      <Collapse in={isOpen} animateOpacity>
        <Button
          type="submit"
          colorScheme="teal"
          isLoading={isPosting}
          onFocus={onOpen}
          onBlur={onClose}
          onClick={onCommentSubmit}
        >
          댓글 작성
        </Button>
      </Collapse>
    </form>
  );
};

export default VideoCommentForm;
