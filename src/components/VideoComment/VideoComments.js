import { Button, Icon, Divider, Flex, VStack, Spinner } from "@chakra-ui/react";
import { useEffect, useRef, useState } from "react";
import { FiMoreVertical } from "react-icons/fi";
import { useUserState } from "../../hooks/UserContext";
import { getComments } from "../../lib/Video";
import VideoComment from "./VideoComment";
import VideoCommentForm from "./VideoCommentForm";
const VideoComments = ({ videoInfo }) => {
  const userInfo = useUserState();
  const [comments, setComments] = useState();
  const [isCommentsLoading, setCommentsLoading] = useState();
  const [isLoadMoreVisible, setLoadMoreVisible] = useState(true);
  const currentPage = useRef(1);

  const loadComments = async (paging) => {
    setCommentsLoading(true);
    let res = await getComments(videoInfo.id, paging);

    if (res.errorMsg) {
      alert("댓글을 가져오는 중 에러가 발생했습니다.");
      return;
    }
    if (res.length === 0) {
      setLoadMoreVisible(false);
    }

    setComments(res);
    setCommentsLoading(false);
  };

  const loadMoreComments = async () => {
    setCommentsLoading(true);
    currentPage.current++;
    let res = await getComments(videoInfo.id, currentPage.current);

    if (res.errorMsg) {
      alert("댓글을 가져오는 중 에러가 발생했습니다.");
      return;
    }

    if (res.length === 0) {
      setLoadMoreVisible(false);
    }

    let comm = [...comments];
    setComments([...comm, ...res]);
    setCommentsLoading(false);
  };

  useEffect(() => {
    loadComments(1);
  }, []);

  const addPostedComment = (val) => {
    let comm = [...comments];

    setComments([
      {
        userId: userInfo.userData.id,
        comment: val,
        insertDate: Date.now() / 1000,
        highlight: true,
      },
      ...comm,
    ]);
  };

  if (typeof videoInfo === "undefined") {
    return null;
  }

  return (
    <>
      <Flex h="40px" alignItems="center">
        댓글 {videoInfo.commentCount}개 <Icon as={FiMoreVertical} />
      </Flex>
      <VideoCommentForm
        videoInfo={videoInfo}
        isReadOnly={isCommentsLoading}
        onSubmit={addPostedComment}
      />
      <VStack mt="4" align="start" divider={<Divider />}>
        {comments ? (
          comments.map((val) => <VideoComment info={val} />)
        ) : (
          <Spinner />
        )}
      </VStack>

      {isLoadMoreVisible && (
        <Button
          onClick={loadMoreComments}
          variant="ghost"
          isLoading={isCommentsLoading}
        >
          더 보기
        </Button>
      )}
    </>
  );
};

export default VideoComments;
