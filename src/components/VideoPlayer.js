import React, { useEffect, useRef } from "react";
import Hls from "hls.js";
import { APIURL } from "../lib/constants";

const VideoPlayer = (props) => {
  const { info, subtitle } = props;

  let video = useRef();

  useEffect(() => {
    if (Hls.isSupported()) {
      let hls = new Hls();
      hls.loadSource(`${APIURL}/video/${info.id}/playlist`);

      window.hls = hls;
      hls.on(Hls.Events.MANIFEST_PARSED, function (event, data) {
        hls.attachMedia(video.current);
      });
    } else {
      alert("browser not supported");
    }
  }, [info]);

  return (
    <video
      style={{
        width: "100%",
        maxHeight: 600,
      }}
      ref={video}
      autoPlay={false}
      crossOrigin="anonymous"
      controls
    >
      {subtitle && (
        <track
          default
          kind="subtitles"
          label={subtitle.title}
          src={subtitle.src}
          srcLang="ko"
        />
      )}
    </video>
  );
};

export default VideoPlayer;
