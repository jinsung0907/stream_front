import {
  Icon,
  Badge,
  Box,
  Flex,
  Text,
  Button,
  useToast,
} from "@chakra-ui/react";
import { useState } from "react";
import { FiCheck, FiThumbsDown, FiThumbsUp } from "react-icons/fi";
import { subtitleDislike, subtitleLike } from "../../lib/Subtitle";

const VideoSubtitleListItem = ({ info, onClick, isOwner, selected }) => {
  const [likeBtn, setLikeBtn] = useState(info.upvote);
  const [dislikeBtn, setDislikeBtn] = useState(info.downvote);
  const [likeBtnLoading, setLikeBtnLoading] = useState(false);

  const toast = useToast();

  const onClickLikeBtn = async (type) => {
    setLikeBtnLoading(true);
    let res;
    if (type) {
      res = await subtitleLike(info.id);
    } else {
      res = await subtitleDislike(info.id);
    }

    if (res.status === "success") {
      setLikeBtn(likeBtn + res.data.like);
      setDislikeBtn(dislikeBtn + res.data.dislike);

      if (res.data.like === -1 && res.data.dislike === 0) {
        toast({
          title: "좋아요 취소.",
          status: "success",
          duration: 1500,
        });
      } else if (res.data.dislike === -1 && res.data.like === 0) {
        toast({
          title: "싫어요 취소.",
          status: "success",
          duration: 1500,
        });
      }
    } else if (res.errorMsg) {
      toast({
        title: "에러발생",
        description: res.errorMsg,
        status: "error",
        duration: 3000,
      });
    } else {
      toast({
        title: "err",
        status: "error",
        duration: 3000,
      });
    }
    setLikeBtnLoading(false);
  };

  return (
    <Flex
      cursor="pointer"
      w="100%"
      h="50px"
      alignItems="center"
      justifyContent="space-between"
    >
      <Flex onClick={onClick} flexGrow="1">
        {selected && (
          <Box alignSelf="center" mr="2">
            <Icon as={FiCheck} />
          </Box>
        )}
        <Box>
          <Text fontWeight="bold">
            {isOwner && <Badge mr="1">업로더</Badge>}
            {info.title}
          </Text>
          <Text isTruncated>{info.userName}</Text>
        </Box>
      </Flex>
      <Box>
        <Button
          onClick={() => onClickLikeBtn(true)}
          leftIcon={<FiThumbsUp />}
          variant="ghost"
          isLoading={likeBtnLoading}
          colorScheme="blue"
        >
          {likeBtn}
        </Button>
        <Button
          onClick={() => onClickLikeBtn(false)}
          leftIcon={<FiThumbsDown />}
          variant="ghost"
          isLoading={likeBtnLoading}
          colorScheme="red"
        ></Button>
      </Box>
    </Flex>
  );
};

export default VideoSubtitleListItem;
