import { Flex } from "@chakra-ui/react";
import VideoPlayer from "../VideoPlayer";
const PlayerArea = (props) => {
  return (
    <Flex>
      <VideoPlayer videoId={props.id} />
    </Flex>
  );
};

export default PlayerArea;
