import { Button, useDisclosure, Flex, VStack } from "@chakra-ui/react";
import { FiPlusSquare } from "react-icons/fi";
import VideoSubtitleUploadModal from "../Modal/VideoSubtitleUploadModal";
import VideoSubtitleListItem from "./VideoSubtitleListItem";

const VideoSubtitleList = ({ info, list, onClick, selected }) => {
  const { isOpen, onOpen, onClose } = useDisclosure();

  const onUpload = () => {
    onClose();
  };

  return (
    <>
      <Flex h="40px" alignItems="center">
        자막 {list.length}개{" "}
        <Button
          variant="ghost"
          size="sm"
          color="black"
          leftIcon={<FiPlusSquare />}
          ml="3"
          onClick={onOpen}
        >
          추가
        </Button>
      </Flex>
      <VStack>
        {list.map((val, i) => (
          <VideoSubtitleListItem
            key={val.id}
            info={val}
            selected={selected && selected.id === val.id}
            index={i}
            isOwner={info.userId === val.userId}
            onClick={() => onClick(i)}
          />
        ))}
      </VStack>
      {isOpen && (
        <VideoSubtitleUploadModal
          info={info}
          isOpen={isOpen}
          onOpen={onOpen}
          onClose={onClose}
          onUpload={onUpload}
        />
      )}
    </>
  );
};

export default VideoSubtitleList;
