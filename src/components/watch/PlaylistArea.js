import { Flex, Box, Text, Divider } from "@chakra-ui/react";
import { useEffect, useRef } from "react";
import VideoListItemSmall from "../Video/VideoListItemSmall";
import ReactDragListView from "react-drag-listview";

const PlaylistArea = (props) => {
  const { playlist, playingVideoInfo } = props;
  const scrollRef = useRef();

  //재생중인 영상 변경시 스크롤 이동
  useEffect(() => {
    if (playingVideoInfo && scrollRef.current) {
      scrollRef.current.scrollIntoView();
    }
  }, [playingVideoInfo]);

  return (
    <Flex
      minW="400px"
      w={["100%", "100%", 400]}
      h="100%"
      maxH="330px"
      m="2"
      p="2"
      borderWidth="1px"
      flexFlow="column"
    >
      <Box mb="3">
        <Text>{playlist.title}</Text>
        <Text color="gray.300">{playlist.userName}</Text>
      </Box>
      <Divider my="2" />

      <ReactDragListView
        onDragEnd={props.onDragEnd}
        handleSelector="li"
        nodeSelector="li"
      >
        {playlist.items.map((val) => (
          <VideoListItemSmall
            key={val.id}
            ref={playingVideoInfo.id === val.videoInfo.id ? scrollRef : null}
            info={val.videoInfo}
            isActive={playingVideoInfo.id === val.videoInfo.id}
          />
        ))}
      </ReactDragListView>

      {/*
      <VStack w="100%" overflowY="auto">
        {playlist.items.map((val) => (
          <VideoListItemSmall
            key={val.id}
            ref={playingVideoInfo.id === val.videoInfo.id && scrollRef}
            info={val.videoInfo}
            isActive={playingVideoInfo.id === val.videoInfo.id}
          />
        ))}
      </VStack>
      */}
    </Flex>
  );
};

export default PlaylistArea;
