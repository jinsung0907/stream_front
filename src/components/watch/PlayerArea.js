import VideoPlayer from "../VideoPlayer";

const PlayerArea = (props) => {
  if (props.isSubLoading) {
    return null;
  } else {
    return <VideoPlayer {...props} />;
  }
};

export default PlayerArea;
