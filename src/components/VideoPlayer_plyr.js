import React, { useEffect, useRef } from "react";
import Plyr from "plyr";
import Hls from "hls.js";
import "plyr/dist/plyr.css";
import { APIURL, CDNURL } from "../lib/constants";

const VideoPlayer = (props) => {
  const { info, subtitle, showSubtitleOption } = props;

  let video = useRef();
  let player = useRef();

  useEffect(() => {
    const defaultOptions = {};

    if (Hls.isSupported()) {
      let hls = new Hls();
      hls.loadSource(`${APIURL}/video/${info.id}/playlist`);

      window.hls = hls;
      hls.on(Hls.Events.MANIFEST_PARSED, function (event, data) {
        // Transform available levels into an array of integers (height values).
        const availableQualities = hls.levels.map((l) => l.height);

        // Add new qualities to option
        defaultOptions.quality = {
          default: availableQualities[0],
          options: availableQualities,
          // this ensures Plyr to use Hls to update quality level
          forced: true,
          onChange: (e) => updateQuality(e),
        };
        defaultOptions.controls = [
          "play-large",
          "play",
          "progress",
          "current-time",
          "mute",
          "volume",
          "captions",
          "settings",
          "pip",
          "airplay",
          "fullscreen",
        ];
        defaultOptions.settings = ["quality", "speed", "pip", "loop"];
        if (showSubtitleOption) {
          defaultOptions.settings.unshift("captions");
        }

        defaultOptions.i18n = {
          restart: "다시보기",
          rewind: "{seektime}초 앞으로",
          play: "재생",
          pause: "일시정지",
          fastForward: " {seektime}초 뒤로",
          seek: "탐색",
          seekLabel: "{currentTime}/{duration}",
          played: "재생함",
          buffered: "Buffered",
          currentTime: "현재시간",
          duration: "길이",
          volume: "볼륨",
          mute: "음소거",
          unmute: "음소거 해제",
          enableCaptions: "자막 켜기",
          disableCaptions: "자막 끄기",
          download: "다운로드",
          enterFullscreen: "전체화면",
          exitFullscreen: "전체화면종료",
          frameTitle: "Player for {title}",
          captions: "자막",
          settings: "설정",
          pip: "PIP",
          menuBack: "이전 메뉴로",
          speed: "속도",
          normal: "정상",
          quality: "화질",
          loop: "반복",
          start: "시작",
          end: "끝",
          all: "전체",
          reset: "리셋",
          disabled: "비활성화",
          enabled: "활성화",
          advertisement: "Ad",
          qualityBadge: {
            2160: "4K",
            1440: "QHD",
            1080: "FHD",
            720: "HD",
            576: "SD",
            480: "SD",
          },
        };

        defaultOptions.captions = {
          active: true,
          language: "auto",
          update: true,
        };

        // Initialize here
        player.current = new Plyr(video.current, defaultOptions);

        player.current.poster = `${CDNURL}/thumbnail/${info.id}.png`;

        hls.attachMedia(video.current);

        player.current.on("languagechange", (e) => {
          console.log(e);
        });
      });
    } else {
      alert("browser not supported");
    }
  }, [info, showSubtitleOption]);

  function updateQuality(newQuality) {
    window.hls.levels.forEach((level, levelIndex) => {
      if (level.height === newQuality) {
        //console.log("Found quality match with " + newQuality);
        window.hls.currentLevel = levelIndex;
      }
    });
  }

  return (
    <video
      style={{ maxWidth: "600px" }}
      ref={video}
      autoPlay={false}
      crossOrigin="anonymous"
    >
      {subtitle && (
        <track
          default
          kind="subtitles"
          label={subtitle.title}
          src={subtitle.src}
          srcLang="ko"
        />
      )}
    </video>
  );
};

export default VideoPlayer;
