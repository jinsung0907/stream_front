import { SimpleGrid, Spinner } from "@chakra-ui/react";
import { throttle } from "lodash";
import { useEffect, useState } from "react";
import { getVideoList } from "../lib/Video";
import VideoItem from "./Video/VideoItem";

const VideoList = (props) => {
  const { videoList: initList } = props;
  const [videoList, setVideoList] = useState(initList);
  const [isLoading, setLoading] = useState(false);
  const [isAddLoading, setAddLoading] = useState(false);
  const [paging, setPaging] = useState(1);
  const [isLastPage, setIsLastPage] = useState(false);

  const getInitialVideos = async () => {
    setLoading(true);
    let res = await getVideoList(1);
    setLoading(false);

    if (res.status === "success") {
      setVideoList(res.data);
    } else {
      console.error(res);
    }
  };

  const getNextVideos = async () => {
    setAddLoading(true);
    let res = await getVideoList(paging + 1);
    setPaging(paging + 1);

    if (res.status === "success") {
      if (res.data.length === 0) {
        setIsLastPage(true);
      }

      setVideoList([...videoList, ...res.data]);
    } else {
      console.error(res);
    }
    setAddLoading(false);
  };

  const onScroll = (e) => {
    if (
      !isAddLoading &&
      !isLastPage &&
      window.innerHeight + window.pageYOffset >=
        document.body.offsetHeight - 200
    ) {
      getNextVideos();
    }
  };

  const onScrollHandler = throttle(onScroll, 500);

  useEffect(() => {
    if (!initList) {
      getInitialVideos();
    }
  }, [initList]);

  useEffect(() => {
    document.addEventListener("scroll", onScrollHandler, true);

    return () => {
      document.removeEventListener("scroll", onScrollHandler, true);
    };
  }, [videoList, isAddLoading, paging, isLastPage]);

  if (isLoading) {
    return <Spinner />;
  }

  return (
    <SimpleGrid
      w="100%"
      h="100%"
      maxW="1300px"
      minChildWidth="240px"
      gap="30px"
      px="3"
      justifyContent="space-between"
      alignItems="center"
    >
      {videoList &&
        videoList.map((val) => <VideoItem key={val.id} info={val} />)}
    </SimpleGrid>
  );
};

export default VideoList;
