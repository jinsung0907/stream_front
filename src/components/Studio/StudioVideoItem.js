import {
  Flex,
  Box,
  Image,
  Text,
  Collapse,
  useDisclosure,
  Icon,
} from "@chakra-ui/react";
import { useEffect, useState } from "react";
import { FiArrowDown } from "react-icons/fi";
import { CDNURL } from "../../lib/constants";
import { getVideoEncodeStatus } from "../../lib/Studio";
import { visibleToString } from "../../lib/Video";

const StudioVideoItem = (props) => {
  let { item } = props;
  const [encode, setEncode] = useState();
  const encodeOpen = useDisclosure();

  useEffect(() => {
    async function _getEncodeStatus() {
      let res = await getVideoEncodeStatus(item.id);

      if (res.status === "success") {
        if (res.isReady === true) {
          setEncode(undefined);
        } else if (res.isReady === false) {
          setEncode(res.data);
        }
      }
    }
    if (item.isReady === false) {
      _getEncodeStatus();
    }
  }, [item]);

  return (
    <Flex
      w="100%"
      py="2"
      _hover={{ background: "gray.200" }}
      onClick={() => props.onClick(item)}
    >
      <Box mr="2">
        <Image w="200px" src={`${CDNURL}/thumbnail/${item.id}.png`} />
      </Box>
      <Box w="100%" justifyContent="flex-start">
        <Text fontSize="1.1rem" fontWeight="bold" mb="1">
          {item.title}
        </Text>
        공개상태 : {visibleToString(item.visibleStatus)}
        {encode && (
          <>
            <Text
              cursor="pointer"
              mt="2"
              onClick={(e) => {
                e.stopPropagation();
                encodeOpen.onToggle();
              }}
            >
              인코딩 진행중
              <Icon as={FiArrowDown} />
            </Text>
            <Collapse in={encodeOpen.isOpen} animateOpacity>
              <Box px="2" bg="gray.300">
                {encode.map((val) => (
                  <Text>
                    {val.resolution}P : {val.progressText}%
                  </Text>
                ))}
              </Box>
            </Collapse>
          </>
        )}
      </Box>
    </Flex>
  );
};

export default StudioVideoItem;
