import dayjs from "dayjs";
import { Link } from "react-router-dom";
import { CDNURL } from "../../lib/constants";
import { Icon, Image, Box } from "@chakra-ui/react";
import { FiPlay } from "react-icons/fi";
import relativeTime from "dayjs/plugin/relativeTime";
import "dayjs/locale/ko";
dayjs.extend(relativeTime);
dayjs.locale("ko");

const PlaylistItem = ({ info }) => {
  if (typeof info === "undefined") {
    return (
      <Box maxW="500px" minW="250px" pb="5" overflow="hidden">
        <Box as={Link} to="#">
          <Box position="relative">
            <Image fallbackSrc="playlist.png" alt="meow" />
            <Box
              position="absolute"
              top="0"
              right="0"
              width="40%"
              height="100%"
              display="flex"
              justifyContent="center"
              alignItems="center"
              backgroundColor="rgba(0,0,0,0.7)"
              color="white"
            >
              <Box fontSize="1.5rem">
                <Icon as={FiPlay} mr="1" /> -
              </Box>
            </Box>
          </Box>

          <Box p="2">
            <Box
              fontWeight="bold"
              mb="1"
              overflow="hidden"
              textOverflow="ellipsis"
              wordBreak="break-all"
              display="-webkit-box"
              style={{
                WebkitLineClamp: 2,
                WebkitBoxOrient: "vertical",
              }}
            >
              "존재하지 않음"
            </Box>

            <Box color="gray.700">-</Box>
          </Box>
        </Box>
      </Box>
    );
  }

  return (
    <Box maxW="500px" minW="250px" pb="5" overflow="hidden">
      <Box
        as={Link}
        to={`/watch/${info.firstItem && info.firstItem.id}?playlistId=${
          info.id
        }`}
      >
        <Box position="relative">
          <Image
            src={`${CDNURL}/thumbnail/${
              info.firstItem && info.firstItem.id
            }.png`}
            fallbackSrc="playlist.png"
            alt="meow"
          />
          <Box
            position="absolute"
            top="0"
            right="0"
            width="40%"
            height="100%"
            display="flex"
            justifyContent="center"
            alignItems="center"
            backgroundColor="rgba(0,0,0,0.7)"
            color="white"
          >
            <Box fontSize="1.5rem">
              <Icon as={FiPlay} mr="1" />
              {info.itemCount}
            </Box>
          </Box>
        </Box>

        <Box p="2">
          <Box
            fontWeight="bold"
            mb="1"
            overflow="hidden"
            textOverflow="ellipsis"
            wordBreak="break-all"
            display="-webkit-box"
            style={{
              WebkitLineClamp: 2,
              WebkitBoxOrient: "vertical",
            }}
          >
            {info.title}
          </Box>

          <Box color="gray.700">{info.userName}</Box>
        </Box>
      </Box>
    </Box>
  );
};

export default PlaylistItem;
