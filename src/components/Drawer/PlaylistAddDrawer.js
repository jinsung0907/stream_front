import {
  Drawer,
  DrawerBody,
  DrawerFooter,
  DrawerHeader,
  DrawerOverlay,
  DrawerContent,
  DrawerCloseButton,
  VStack,
  Box,
  Flex,
  Spinner,
  Button,
  Checkbox,
  useDisclosure,
  useToast,
  Icon,
  IconButton,
} from "@chakra-ui/react";
import { useEffect, useState } from "react";
import {
  addPlayListItem,
  deletePlayListItem,
  getUserPlaylists,
} from "../../lib/Playlist";
import CreatePlaylistModal from "../Modal/CreatePlaylistModal";
import { FiGlobe, FiLink2, FiLock, FiMoreVertical } from "react-icons/fi";
import ModifyPlaylistModal from "../Modal/ModifyPlaylistModal";

const PlaylistAddDrawer = ({
  isOpen,
  onClose,
  onOpen,
  videoInfo,
  userInfo,
}) => {
  const [isLoading, setLoading] = useState();
  const [playlist, setPlaylist] = useState();
  const [modifySelect, setModifySelect] = useState();
  const isCreateOpen = useDisclosure();
  const isModifyOpen = useDisclosure();
  const toast = useToast();

  const onCreatePlaylist = (e) => {
    isCreateOpen.onClose();
    getPlaylists();
  };

  async function onClickItem(item, e) {
    //체크 안되어있으면 재생목록에 추가, 되어있으면 삭제
    //이미 값이 변한 뒤이므로 반대로
    if (e.target.checked) {
      let result = await addPlayListItem(item.id, videoInfo.id);

      if (result.status === "success") {
        toast({
          title: `"${item.title}"에 추가됨`,
          status: "success",
          duration: 3000,
        });

        // 해당 isAdded값 추가된 id로 변경
        let tmp = playlist.map((val) => {
          if (val.id === item.id) {
            val.isAdded = result.data.id;
          }
          return val;
        });
        setPlaylist(tmp);
      } else if (result.errorMsg) {
        toast({
          title: "에러 발생",
          status: "error",
          description: result.errorMsg,
          duration: 3000,
        });
      }
    } else {
      let result = await deletePlayListItem(item.isAdded);

      if (result.status === "success") {
        toast({
          title: `"${item.title}"에서 삭제됨`,
          status: "success",
          duration: 3000,
        });
        // 해당 isAdded값 삭제
        let tmp = playlist.map((val) => {
          if (val.id === item.id) {
            val.isAdded = false;
          }
          return val;
        });
        setPlaylist(tmp);
      } else if (result.errorMsg) {
        toast({
          title: "에러 발생",
          status: "error",
          description: result.errorMsg,
          duration: 3000,
        });
      }
    }
  }

  async function getPlaylists() {
    try {
      setLoading(true);
      let list = await getUserPlaylists(videoInfo.id);
      setPlaylist(list.data);
      setLoading(false);
    } catch (e) {
      setLoading(false);
    }
  }

  const onClickModifyButton = (val, e) => {
    setModifySelect(val.id);
    isModifyOpen.onOpen();
  };

  const onModify = () => {
    getPlaylists();
    isModifyOpen.onClose();
  };

  useEffect(() => {
    getPlaylists();
  }, []);

  return (
    <Drawer placement="bottom" onClose={onClose} isOpen={isOpen} size="md">
      <DrawerOverlay />
      <DrawerContent>
        <DrawerCloseButton />
        <DrawerHeader>재생목록에 추가</DrawerHeader>
        <DrawerBody>
          {isLoading ? (
            <Spinner />
          ) : (
            playlist && (
              <VStack>
                {playlist.length === 0 && (
                  <Box>생성된 재생목록이 없습니다.</Box>
                )}

                {playlist.map((val) => (
                  <Flex
                    key={val.id}
                    w="100%"
                    h="30px"
                    justifyContent="space-between"
                    alignItems="center"
                  >
                    <Checkbox
                      size="lg"
                      isChecked={val.isAdded}
                      onChange={(e) => onClickItem(val, e)}
                    >
                      {val.visibleStatus === "public" && (
                        <Icon as={FiGlobe} mr="1" />
                      )}
                      {val.visibleStatus === "unlisted" && (
                        <Icon as={FiLink2} mr="1" />
                      )}
                      {val.visibleStatus === "private" && (
                        <Icon as={FiLock} mr="1" />
                      )}
                      {val.title}
                    </Checkbox>
                    <IconButton
                      size="md"
                      isRound
                      variant="ghost"
                      icon={<FiMoreVertical />}
                      onClick={(e) => onClickModifyButton(val, e)}
                    />
                  </Flex>
                ))}
              </VStack>
            )
          )}
        </DrawerBody>
        <DrawerFooter>
          <Button onClick={isCreateOpen.onOpen} variant="ghost">
            새 재생목록
          </Button>
        </DrawerFooter>
      </DrawerContent>
      {isCreateOpen.isOpen && (
        <CreatePlaylistModal
          onCreate={onCreatePlaylist}
          isOpen={isCreateOpen.isOpen}
          onClose={isCreateOpen.onClose}
        />
      )}
      {isModifyOpen.isOpen && (
        <ModifyPlaylistModal
          {...isModifyOpen}
          playlistId={modifySelect}
          onModify={onModify}
        />
      )}
    </Drawer>
  );
};

export default PlaylistAddDrawer;
