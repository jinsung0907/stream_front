import {
  AlertIcon,
  Alert,
  Button,
  FormErrorMessage,
  FormControl,
  FormLabel,
  Input,
  Box,
  Flex,
  Text,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
} from "@chakra-ui/react";

import { useRef, useState } from "react";
import { useUserDispatch } from "../hooks/UserContext";
import { doLogin } from "../lib/User";

const LoginModal = ({
  isOpen,
  onOpen,
  onClose,
  isRegister,
  onClickRegister,
}) => {
  const [isLoading, setLoading] = useState(false);
  const [errorForm, setErrorForm] = useState([false, false]);
  const [errorMsg, setErr] = useState();
  const initialRef = useRef();
  const userDispatch = useUserDispatch();

  const onClickLogin = async (event) => {
    event.preventDefault();
    let email = document.getElementById("email").value;
    let password = document.getElementById("password").value;

    let valid = [false, false];
    if (email.length === 0) {
      valid[0] = true;
    }
    if (password.length === 0) {
      valid[1] = true;
    }
    setErrorForm(valid);
    if (valid[0] || valid[1]) return;

    try {
      setLoading(true);
      let res = await doLogin(email, password);
      setLoading(false);

      switch (res.status) {
        case "success":
          userDispatch({ type: "LOGIN", data: res.data, token: res.token });
          onClose();
          break;
        case "noemail":
          setErr("이메일을 입력해주세요.");
          break;
        case "nopassword":
          setErr("비밀번호를 입력해주세요.");
          break;
        case "loginfail":
          setErr("로그인에 실패했습니다. 이메일/비밀번호를 확인해주세요.");
          break;
        default:
          setErr("오류 발생.");
      }
    } catch (e) {
      setErr(e);
    }
  };

  return (
    <>
      <Modal initialFocusRef={initialRef} isOpen={isOpen} onClose={onClose}>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>로그인</ModalHeader>
          <ModalCloseButton />
          <form onSubmit={onClickLogin}>
            <ModalBody pb={6}>
              {isRegister && (
                <Alert status="success" mb="3">
                  <AlertIcon />
                  회원가입이 완료되었습니다. 로그인해주세요.
                </Alert>
              )}
              <FormControl
                isRequired
                isReadOnly={isLoading}
                isInvalid={errorForm[0]}
              >
                <FormLabel>이메일</FormLabel>
                <Input id="email" ref={initialRef} placeholder="이메일" />
                <FormErrorMessage>이메일을 입력해주세요.</FormErrorMessage>
              </FormControl>

              <FormControl
                isRequired
                isReadOnly={isLoading}
                isInvalid={errorForm[1]}
                mt={4}
              >
                <FormLabel>비밀번호</FormLabel>
                <Input id="password" type="password" placeholder="비밀번호" />
                <FormErrorMessage>비밀번호를 입력해주세요.</FormErrorMessage>
              </FormControl>
              {errorMsg && (
                <Text mt="6" color="red.500">
                  {errorMsg}
                </Text>
              )}
            </ModalBody>

            <ModalFooter>
              <Flex w="100%" justifyContent="space-between">
                <Box>
                  <Button onClick={onClickRegister}>회원가입</Button>
                </Box>
                <Box>
                  <Button
                    colorScheme="blue"
                    mr={3}
                    onClick={onClickLogin}
                    isLoading={isLoading}
                    type="submit"
                  >
                    로그인
                  </Button>
                  <Button onClick={onClose}>취소</Button>
                </Box>
              </Flex>
            </ModalFooter>
          </form>
        </ModalContent>
      </Modal>
    </>
  );
};

export default LoginModal;
