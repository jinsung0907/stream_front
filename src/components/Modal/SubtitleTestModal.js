import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalCloseButton,
  ModalBody,
} from "@chakra-ui/modal";
import VideoPlayer from "../VideoPlayer";

const SubtitleTestModal = ({ isOpen, onClose, info, subtitleData }) => {
  return (
    <Modal isOpen={isOpen} onClose={onClose}>
      <ModalOverlay />
      <ModalContent>
        <ModalHeader>영상 정보 수정</ModalHeader>
        <ModalCloseButton />
        <ModalBody>
          <VideoPlayer info={info} subtitle={subtitleData} />
        </ModalBody>
      </ModalContent>
    </Modal>
  );
};

export default SubtitleTestModal;
