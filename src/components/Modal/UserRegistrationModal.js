import {
  Button,
  FormErrorMessage,
  FormControl,
  FormLabel,
  Input,
  Text,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
} from "@chakra-ui/react";
import { useRef, useState } from "react";
import { doRegister } from "../../lib/User";

const UserRegistrationModal = ({ isOpen, onOpen, onClose, onRegister }) => {
  const [isLoading, setLoading] = useState(false);
  const [errorForm, setErrorForm] = useState([false, false]);
  const [errorMsg, setErr] = useState();
  const initialRef = useRef();

  const onClickRegister = async (event) => {
    event.preventDefault();
    let email = document.getElementById("email").value;
    let name = document.getElementById("name").value;
    let password = document.getElementById("password").value;
    let password2 = document.getElementById("password2").value;

    let valid = [false, false, false, false];
    if (email.length === 0) {
      valid[0] = "이메일을 입력해주세요.";
    }
    if (name.length === 0) {
      valid[1] = "닉네임을 입력해주세요.";
    }
    if (password.length === 0) {
      valid[2] = "비밀번호를 입력해주세요.";
    }
    if (password2.length === 0) {
      valid[3] = "비밀번호 확인을 입력해주세요.";
    }
    if (password !== password2) {
      valid[3] = "비밀번호 확인이 일치하지 않습니다.";
      return;
    }

    setErrorForm(valid);
    if (valid[0] || valid[1] || valid[2] || valid[3]) return;

    try {
      setLoading(true);
      let res = await doRegister({
        email: email,
        password: password,
        name: name,
      });
      setLoading(false);

      switch (res.status) {
        case "success":
          onRegister();
          onClose();
          break;
        case "noemail":
          setErr("이메일을 입력해주세요.");
          break;
        case "noname":
          setErr("닉네임을 입력해주세요.");
          break;
        case "nopassword":
          setErr("비밀번호를 입력해주세요.");
          break;
        case "invalidname":
          setErr("닉네임 규칙에 맞게 작성해주세요.");
          break;
        default:
          setErr(res.errorMsg);
      }
    } catch (e) {
      setErr(e);
    }
  };

  return (
    <>
      <Modal initialFocusRef={initialRef} isOpen={isOpen} onClose={onClose}>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>회원가입</ModalHeader>
          <ModalCloseButton />
          <form onSubmit={onClickRegister}>
            <ModalBody pb={6}>
              <FormControl
                isRequired
                isReadOnly={isLoading}
                isInvalid={errorForm[0]}
              >
                <FormLabel>이메일</FormLabel>
                <Input id="email" ref={initialRef} placeholder="이메일" />
                <FormErrorMessage>{errorForm[0]}</FormErrorMessage>
              </FormControl>

              <FormControl
                isRequired
                isReadOnly={isLoading}
                isInvalid={errorForm[1]}
                mt={4}
              >
                <FormLabel>닉네임</FormLabel>
                <Input id="name" placeholder="닉네임 (12자 이하 한영숫자)" />
                <FormErrorMessage>{errorForm[1]}</FormErrorMessage>
              </FormControl>

              <FormControl
                isRequired
                isReadOnly={isLoading}
                isInvalid={errorForm[2]}
                mt={4}
              >
                <FormLabel>비밀번호</FormLabel>
                <Input id="password" type="password" placeholder="비밀번호" />
                <FormErrorMessage>{errorForm[2]}</FormErrorMessage>
              </FormControl>
              <FormControl
                isRequired
                isReadOnly={isLoading}
                isInvalid={errorForm[3]}
                mt={4}
              >
                <FormLabel>비밀번호 확인</FormLabel>
                <Input
                  id="password2"
                  type="password"
                  placeholder="비밀번호 확인"
                />
                <FormErrorMessage>{errorForm[3]}</FormErrorMessage>
              </FormControl>
              {errorMsg && (
                <Text mt="6" color="red.500">
                  {errorMsg}
                </Text>
              )}
            </ModalBody>

            <ModalFooter>
              <Button
                colorScheme="blue"
                mr={3}
                onClick={onClickRegister}
                isLoading={isLoading}
                type="submit"
              >
                회원가입
              </Button>
              <Button onClick={onClose}>취소</Button>
            </ModalFooter>
          </form>
        </ModalContent>
      </Modal>
    </>
  );
};

export default UserRegistrationModal;
