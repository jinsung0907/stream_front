import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalCloseButton,
  ModalBody,
  ModalFooter,
  Button,
  FormControl,
  FormLabel,
  Input,
  Select,
  Spinner,
  Textarea,
  useToast,
} from "@chakra-ui/react";
import { useState } from "react";
import usePlaylistInfo from "../../hooks/usePlaylistInfo";
import { formdataToObject } from "../../lib/common";
import { createPlaylist, deletePlaylist } from "../../lib/Playlist";

const ModifyPlaylistModal = ({ playlistId, isOpen, onClose, onModify }) => {
  const [playlistInfo, isInfoLoading] = usePlaylistInfo(playlistId);
  const [isLoading, setLoading] = useState();
  const toast = useToast();

  const onSubmit = async (e) => {
    try {
      e.preventDefault();

      setLoading(true);
      let formdata = formdataToObject(new FormData(e.target));

      let res = await createPlaylist(formdata);
      setLoading(false);

      if (res.status === "success") {
        toast({
          title: "수정 완료",
          status: "success",
          duration: 2000,
        });
        onModify(res.data);
      } else if (res.errorMsg) {
        toast({
          title: "에러 발생",
          description: res.errorMsg,
          status: "error",
          duration: 3000,
        });
      }
    } catch (e) {
      setLoading(false);
    }
  };

  const onClickDelete = async (e) => {
    if (window.confirm("플레이리스트를 삭제하시겠습니까?")) {
      setLoading(true);
      let res = await deletePlaylist(playlistInfo.id);
      setLoading(false);
      onModify();

      if (res.status === "success") {
        toast({
          title: "삭제 완료",
          status: "success",
          duration: 2000,
        });
      } else if (res.errorMsg) {
        toast({
          title: "에러 발생",
          description: res.errorMsg,
          status: "error",
          duation: 3000,
        });
      } else {
        toast({
          title: "GENERIC ERROR",
          status: "error",
          duation: 3000,
        });
      }
    }
  };

  if (isInfoLoading) {
    return (
      <Modal isOpen={isOpen} onClose={onClose}>
        <ModalOverlay />
        <ModalContent>
          <Spinner />
        </ModalContent>
      </Modal>
    );
  }

  return (
    <Modal isOpen={isOpen} onClose={onClose}>
      <ModalOverlay />
      <ModalContent>
        <ModalHeader>재생목록 정보 수정</ModalHeader>
        <ModalCloseButton />
        <form onSubmit={onSubmit}>
          <ModalBody>
            <FormControl isReadOnly={isLoading}>
              <FormLabel>제목</FormLabel>
              <Input defaultValue={playlistInfo.title} name="title" />
            </FormControl>
            <FormControl isReadOnly={isLoading}>
              <FormLabel>설명</FormLabel>
              <Textarea
                defaultValue={playlistInfo.description}
                name="description"
              />
            </FormControl>
            <FormControl isReadOnly={isLoading}>
              <FormLabel>공개설정</FormLabel>
              <Select
                name="visibleStatus"
                defaultValue={playlistInfo.visibleStatus}
              >
                <option value="public">공개</option>
                <option value="unlisted">미공개</option>
                <option value="private">비공개</option>
              </Select>
            </FormControl>
          </ModalBody>
          <ModalFooter>
            <Button
              type="submit"
              colorScheme="teal"
              isLoading={isLoading}
              mr="2"
            >
              수정
            </Button>
            <Button
              colorScheme="red"
              isLoading={isLoading}
              mr="2"
              onClick={onClickDelete}
            >
              삭제
            </Button>
            <Button onClick={onClose}>취소</Button>
          </ModalFooter>
        </form>
      </ModalContent>
    </Modal>
  );
};

export default ModifyPlaylistModal;
