import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalCloseButton,
  ModalBody,
  ModalFooter,
} from "@chakra-ui/modal";
import {
  Button,
  FormControl,
  FormLabel,
  Input,
  Select,
  Textarea,
  useToast,
} from "@chakra-ui/react";
import { useState } from "react";
import { formdataToObject } from "../../lib/common";
import { createPlaylist } from "../../lib/Playlist";

const CreatePlaylistModal = ({ isOpen, onClose, onCreate }) => {
  const [isLoading, setLoading] = useState();
  const toast = useToast();

  const onSubmit = async (e) => {
    try {
      e.preventDefault();

      setLoading(true);
      let formdata = formdataToObject(new FormData(e.target));

      let res = await createPlaylist(formdata);
      setLoading(false);

      if (res.status === "success") {
        toast({
          title: "재생목록 생성 완료",
          status: "success",
          duration: 2000,
        });
        onCreate(res.data);
      } else if (res.errorMsg) {
        toast({
          title: "에러발생",
          description: res.errorMsg,
          status: "error",
          duration: 3000,
        });
      }
    } catch (e) {
      setLoading(false);
    }
  };

  return (
    <Modal isOpen={isOpen} onClose={onClose}>
      <ModalOverlay />
      <ModalContent>
        <ModalHeader>새 재생목록</ModalHeader>
        <ModalCloseButton />
        <form onSubmit={onSubmit}>
          <ModalBody>
            <FormControl isReadOnly={isLoading}>
              <FormLabel>제목</FormLabel>
              <Input name="title" />
            </FormControl>
            <FormControl isReadOnly={isLoading}>
              <FormLabel>설명</FormLabel>
              <Textarea name="description" />
            </FormControl>
            <FormControl isReadOnly={isLoading}>
              <FormLabel>공개설정</FormLabel>
              <Select name="visibleStatus" defaultValue="public">
                <option value="public">공개</option>
                <option value="unlisted">미공개</option>
                <option value="private">비공개</option>
              </Select>
            </FormControl>
          </ModalBody>
          <ModalFooter>
            <Button
              type="submit"
              colorScheme="teal"
              isLoading={isLoading}
              mr="2"
            >
              생성
            </Button>
            <Button onClick={onClose}>취소</Button>
          </ModalFooter>
        </form>
      </ModalContent>
    </Modal>
  );
};

export default CreatePlaylistModal;
