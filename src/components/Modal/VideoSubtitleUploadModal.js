import {
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
  FormControl,
  FormErrorMessage,
  FormLabel,
  Input,
  Button,
  Box,
  Textarea,
  useDisclosure,
} from "@chakra-ui/react";
import { useState } from "react";
import { APIURL } from "../../lib/constants";
import Cookies from "js-cookie";
import SubtitleTestModal from "./SubtitleTestModal";
import subsrt from "subsrt";

const VideoSubtitleUploadModal = ({
  info,
  isOpen,
  onOpen,
  onClose,
  onUpload,
}) => {
  const [inputError, setInputError] = useState({});
  const [isUploading, setUploading] = useState(false);
  const [subtitleFile, setSubtitleFile] = useState();
  const TestModal = useDisclosure();

  const onChangeSubtitleFile = (e) => {
    let reader = new FileReader();

    reader.addEventListener(
      "load",
      function () {
        let source = reader.result;
        if (subsrt.detect(source) !== "vtt") {
          // 파일형식이 VTT가 아니면 vtt로 변환
          source = subsrt.convert(source, { format: "vtt" });
        }

        setSubtitleFile({
          title: "테스트",
          description: "테스트",
          src:
            "data:text/plain;base64," +
            btoa(unescape(encodeURIComponent(source))),
        });
      },
      false
    );

    if (e.target.files[0]) reader.readAsText(e.target.files[0]);
  };

  const onSubmit = async (e) => {
    e.preventDefault();
    let formData = new FormData(e.target);
    let xhr = new XMLHttpRequest();

    // validation 시작
    let isErr = false;
    setInputError({});
    let errorValue = {};
    if (!formData.get("title")) {
      errorValue = { ...errorValue, title: "제목을 입력하세요." };
      isErr = true;
    }
    if (!formData.get("description")) {
      errorValue = { ...errorValue, description: "설명을 입력하세요." };
      isErr = true;
    }
    if (formData.get("subtitle").name === "") {
      errorValue = { ...errorValue, video: "파일을 선택하세요." };
      isErr = true;
    }
    if (isErr) {
      setInputError(errorValue);
      return;
    }
    // validation 끝

    setUploading(true);

    xhr.open("POST", APIURL + "/subtitle/upload");
    xhr.setRequestHeader("Authorization", "Bearer " + Cookies.get("token"));
    xhr.addEventListener("loadend", function () {
      console.log(xhr.upload.responseText);
      console.log(xhr.response);
      let res = JSON.parse(xhr.responseText);
      if (res.status === "success") {
        alert("업로드 완료.");
        onUpload();
      } else if (res.errorMsg) {
        alert(res.errorMsg);
      } else {
        alert("GENERIC ERROR");
      }
      setUploading(false);
    });
    /*
    xhr.upload.addEventListener("progress", function (event) {
      if (event.lengthComputable) {
        var complete = ((event.loaded / event.total) * 100) | 0;
        setUploadPercent(complete);
      }
    });
    */
    xhr.send(formData);
  };

  return (
    <Modal isOpen={isOpen} onClose={onClose}>
      <ModalOverlay />
      <ModalContent>
        <ModalHeader>자막 업로드</ModalHeader>
        <ModalCloseButton />
        <ModalBody>
          <form onSubmit={onSubmit}>
            <input type="hidden" name="videoId" defaultValue={info.id} />
            <FormControl isRequired isInvalid={inputError.subtitle} mt="3">
              <FormLabel>자막 파일</FormLabel>
              <Input
                onChange={onChangeSubtitleFile}
                name="subtitle"
                type="file"
              />
              <FormErrorMessage>{inputError.subtitle}</FormErrorMessage>
            </FormControl>
            <FormControl
              isRequired
              isReadOnly={isUploading}
              isInvalid={inputError.title}
              mt="3"
            >
              <FormLabel>제목</FormLabel>
              <Input name="title" />
              <FormErrorMessage>{inputError.title}</FormErrorMessage>
            </FormControl>
            <FormControl
              isRequired
              isReadOnly={isUploading}
              isInvalid={inputError.description}
              mt="3"
            >
              <FormLabel>설명</FormLabel>
              <Textarea
                placeholder="자막에 대한 간단한 설명"
                name="description"
              />
              <FormErrorMessage>{inputError.description}</FormErrorMessage>
            </FormControl>
            <Box mt="3">
              <Button
                colorScheme="teal"
                type="submit"
                isLoading={isUploading}
                mr="2"
              >
                업로드
              </Button>
              <Button colorScheme="facebook" onClick={TestModal.onOpen} mr="2">
                테스트
              </Button>
              <Button onClick={onClose}>닫기</Button>
            </Box>
          </form>
        </ModalBody>
        <ModalFooter></ModalFooter>
      </ModalContent>
      <SubtitleTestModal
        info={info}
        isOpen={TestModal.isOpen}
        onClose={TestModal.onClose}
        subtitleData={subtitleFile}
      />
    </Modal>
  );
};

export default VideoSubtitleUploadModal;
