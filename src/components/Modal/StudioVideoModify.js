import {
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
  Select,
  Textarea,
  Button,
  FormControl,
  FormLabel,
  Image,
  Input,
  Box,
} from "@chakra-ui/react";
import { useState } from "react";
import { formdataToObject } from "../../lib/common";
import { CDNURL } from "../../lib/constants";
import { modifyVideoInfo } from "../../lib/Video";

const StudioVideoModify = ({ info, isOpen, onOpen, onClose, onModified }) => {
  const [isSubmit, setSubmit] = useState(false);
  const onSubmit = async (e) => {
    e.preventDefault();

    let formdata = formdataToObject(new FormData(e.target));
    formdata.videoId = info.id;

    setSubmit(true);
    let res = await modifyVideoInfo(formdata);
    setSubmit(false);

    if (res.status === "success") {
      onModified();
      onClose();
    } else if (res.errorMsg) {
      alert(res.errorMsg);
    } else {
      alert("GENERIC ERROR");
    }
  };
  return (
    <Modal isOpen={isOpen} onClose={onClose}>
      <ModalOverlay />
      <ModalContent>
        <ModalHeader>영상 정보 수정</ModalHeader>
        <ModalCloseButton />
        <ModalBody>
          <Image src={`${CDNURL}/thumbnail/${info.id}.png`} mb="3" />
          <form onSubmit={onSubmit}>
            <FormControl isReadOnly={isSubmit}>
              <FormLabel>제목</FormLabel>
              <Input name="title" defaultValue={info.title} />
            </FormControl>
            <FormControl isReadOnly={isSubmit}>
              <FormLabel>설명</FormLabel>
              <Textarea name="description" defaultValue={info.description} />
            </FormControl>
            <FormControl isReadOnly={isSubmit}>
              <FormLabel>공개설정</FormLabel>
              <Select name="visibleStatus" defaultValue={info.visibleStatus}>
                <option value="public">공개</option>
                <option value="unlisted">미공개</option>
                <option value="private">비공개</option>
              </Select>
            </FormControl>
            <Box mt="3">
              <Button colorScheme="teal" type="submit">
                수정
              </Button>
              <Button onClick={onClose}>닫기</Button>
            </Box>
          </form>
        </ModalBody>
        <ModalFooter></ModalFooter>
      </ModalContent>
    </Modal>
  );
};

export default StudioVideoModify;
