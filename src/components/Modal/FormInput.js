import {
  FormControl,
  FormLabel,
  FormErrorMessage,
  Input,
} from "@chakra-ui/react";
import { useEffect, useState } from "react";

const FormInput = (props) => {
  const [value, setValue] = useState(props.value);
  const [errorMsg, setErrorMsg] = useState(props.errorMsg);
  const [isInvalid, setInvalid] = useState(props.isInvalid);
  const [placeholder, setPlaceholder] = useState(props.placeholder);

  const onFocus = (e) => {};

  const onChange = (e) => {
    setValue(e.target.value);
    props.onChange(e.target.value);
  };

  useEffect(() => {
    if (value !== props.value) {
      setValue(props.value);
    }
    if (errorMsg !== props.errorMsg) {
      setErrorMsg(props.errorMsg);
    }
    if (isInvalid !== props.isInvalid) {
      setInvalid(props.isInvalid);
    }
    if (placeholder !== props.placeholder) {
      setPlaceholder(props.placeholder);
    }
  }, [props.errorMsg, props.isInvalid, props.placeholder, props.value]);

  return (
    <FormControl isRequired isReadOnly={props.isLoading} isInvalid={isInvalid}>
      <FormLabel>이메일</FormLabel>
      <Input
        value={value}
        onFocus={onFocus}
        onChange={onChange}
        placeholder={placeholder}
      />
      <FormErrorMessage>이메일을 입력해주세요.</FormErrorMessage>
    </FormControl>
  );
};

export default FormInput;
