import { VStack } from "@chakra-ui/react";
import VideoListItem from "./VideoListItem";

const VideoList = ({ items }) => {
  return (
    <VStack>
      {items.map((val) => (
        <VideoListItem info={val} />
      ))}
    </VStack>
  );
};

export default VideoList;
