import { Image, Flex, Box } from "@chakra-ui/react";
import dayjs from "dayjs";
import { Link } from "react-router-dom";
import relativeTime from "dayjs/plugin/relativeTime";
import "dayjs/locale/ko";
import { CDNURL } from "../../lib/constants";
dayjs.extend(relativeTime);
dayjs.locale("ko");

const VideoListItemSmall = ({ loading, info, isActive, ref }) => {
  return (
    <Box
      as="li"
      listStyleType="none"
      mb="1"
      maxW="sm"
      /* borderWidth="1px" */ borderRadius="lg"
      _hover={{ background: "rgba(0, 0, 0, 0.05)" }}
      background={isActive && "rgba(0,0,0,0.1)"}
      ref={ref && ref}
    >
      <Box as={Link} to={`/watch/${info.id}`}>
        <Flex h="100">
          <Image src={`${CDNURL}/thumbnail/${info.id}.png`} alt="nya" mr="3" />

          <Flex
            flexDir="column"
            justifyContent="center"
            p="1"
            fontSize="0.8rem"
          >
            <Box fontSize="1rem" fontWeight="bold" mb="1">
              {info.title}
            </Box>

            <Box color="gray.700">{info.userName}</Box>
            <Box color="gray.700">
              조회수 {info.viewCount}회 -{" "}
              {dayjs.unix(info.publicDate).fromNow()}
            </Box>
          </Flex>
        </Flex>
      </Box>
    </Box>
  );
};

export default VideoListItemSmall;
