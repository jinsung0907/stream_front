import { Image, Flex, Box } from "@chakra-ui/react";
import dayjs from "dayjs";
import { Link } from "react-router-dom";
import relativeTime from "dayjs/plugin/relativeTime";
import "dayjs/locale/ko";
import { CDNURL } from "../../lib/constants";
dayjs.extend(relativeTime);
dayjs.locale("ko");

const VideoListItem = ({ loading, info }) => {
  return (
    <Box borderWidth="1px" borderRadius="lg">
      <Box as={Link} to={`/watch/${info.id}`}>
        <Flex h="200">
          <Image src={`${CDNURL}/thumbnail/${info.id}.png`} alt="nya" />

          <Flex
            flexDir="column"
            justifyContent="center"
            p="3"
            fontSize="0.8rem"
          >
            <Box fontWeight="bold" mb="1">
              {info.title}
            </Box>

            <Box color="gray.700">{info.userName}</Box>
            <Box color="gray.700">
              조회수 {info.viewCount}회 -{" "}
              {dayjs.unix(info.publicDate).fromNow()}
            </Box>
          </Flex>
        </Flex>
      </Box>
    </Box>
  );
};

export default VideoListItem;
