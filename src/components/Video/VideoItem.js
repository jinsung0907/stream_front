import { Image, Box } from "@chakra-ui/react";
import dayjs from "dayjs";
import { Link } from "react-router-dom";
import relativeTime from "dayjs/plugin/relativeTime";
import "dayjs/locale/ko";
import { lengthToDisplay } from "../../lib/Video";
import { CDNURL } from "../../lib/constants";
dayjs.extend(relativeTime);
dayjs.locale("ko");

const VideoItem = ({ loading, info }) => {
  return (
    <Box maxW="500px" minW="250px" pb="5" overflow="hidden">
      <Box as={Link} to={`/watch/${info.id}`}>
        <Box position="relative">
          <Image src={`${CDNURL}/thumbnail/${info.id}.png`} alt="meow" />
          <Box
            position="absolute"
            right="5px"
            bottom="5px"
            borderRadius="5px"
            backgroundColor="blackAlpha.900"
            color="white"
            fontSize="12px"
            letterSpacing="0.4px"
            fontWeight="500"
            px="5px"
            lineHeight="18px"
            textAlign="center"
          >
            {lengthToDisplay(info.videoLength)}
          </Box>
        </Box>

        <Box p="2">
          <Box
            fontWeight="bold"
            mb="1"
            overflow="hidden"
            textOverflow="ellipsis"
            wordBreak="break-all"
            display="-webkit-box"
            style={{
              WebkitLineClamp: 2,
              WebkitBoxOrient: "vertical",
            }}
          >
            {info.title}
          </Box>

          <Box color="gray.700">{info.userName}</Box>
          <Box fontSize="12px" color="gray.700">
            조회수 {info.viewCount}회 - {dayjs.unix(info.publicDate).fromNow()}
          </Box>
        </Box>
      </Box>
    </Box>
  );
};

export default VideoItem;
