import {
  Button,
  useDisclosure,
  Input,
  InputGroup,
  InputRightElement,
  Link,
  Flex,
  Box,
} from "@chakra-ui/react";
import { DragHandleIcon, SearchIcon } from "@chakra-ui/icons";
import { useState } from "react";
import { Link as LinkR, useHistory } from "react-router-dom";
import { useUserDispatch, useUserState } from "../../hooks/UserContext";
import LoginModal from "../LoginModal";
import UserRegistrationModal from "../Modal/UserRegistrationModal";

const Navbar = (props) => {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const {
    isOpen: isRegOpen,
    onOpen: onRegOpen,
    onClose: onRegClose,
  } = useDisclosure();
  const userInfo = useUserState();
  const userDispatch = useUserDispatch();
  const history = useHistory();

  const [isRegister, setRegister] = useState(false);
  const [searchstr, setSearchstr] = useState(props.searchstr);

  const onRegister = () => {
    onRegClose();
    setRegister(true);
    onOpen();
  };
  const onClickRegister = () => {
    onClose();
    onRegOpen();
  };

  const onSearch = () => {
    let searchstr = document.getElementById("searchInput").value;
    history.push("/search/" + searchstr);
  };

  return (
    <Box w="100%" h="50px" backgroundColor="orange">
      <Flex
        w="100%"
        h="100%"
        justifyContent="space-between"
        alignItems="center"
        px="5"
      >
        <Flex h="100%" justifyContent="space-between" alignItems="center">
          {/*
          <Button variant="link" mr="3">
            <HamburgerIcon w="6" h="6" color="black" />
          </Button>
          */}
          <Link fontWeight="bold" as={LinkR} to="/" mr="3">
            메인으로
          </Link>

          {userInfo.logined && (
            <Link fontWeight="bold" as={LinkR} to="/upload" mr="3">
              업로드
            </Link>
          )}
        </Flex>

        <Flex h="100%" justifyContent="space-between" alignItems="center">
          <InputGroup>
            <Input
              id="searchInput"
              bg="white"
              onKeyPress={(e) => e.key === "Enter" && onSearch()}
              placeholder="검색"
              value={searchstr}
              onChange={(e) => setSearchstr(e.target.value)}
            />
            <InputRightElement
              cursor="pointer"
              onClick={onSearch}
              children={<SearchIcon color="gray.300" />}
            />
          </InputGroup>
        </Flex>

        <Flex>
          {userInfo.logined ? (
            <>
              <Link as={LinkR} to="/studio" mr="3">
                studio
              </Link>
              <Flex
                cursor="pointer"
                onClick={() => userDispatch({ type: "LOGOUT" })}
              >
                {userInfo.userData.name}
              </Flex>
            </>
          ) : (
            <Flex>
              <Button variant="link" colorScheme="blue" mr="2">
                <DragHandleIcon w="4" h="4" color="black" />
              </Button>
              <Button
                fontWeight="bold"
                variant="outline"
                colorScheme="blue"
                onClick={onOpen}
              >
                로그인
              </Button>
            </Flex>
          )}
        </Flex>
      </Flex>

      <LoginModal
        isOpen={isOpen}
        onOpen={onOpen}
        onClose={onClose}
        isRegister={isRegister}
        onClickRegister={onClickRegister}
      />
      <UserRegistrationModal
        isOpen={isRegOpen}
        onOpen={onRegOpen}
        onClose={onRegClose}
        onRegister={onRegister}
      />
    </Box>
  );
};

export default Navbar;
