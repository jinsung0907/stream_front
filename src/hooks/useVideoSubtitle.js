import { useEffect, useState } from "react";
import { getVideoSubtitles } from "../lib/Video";

const useVideoSubtitle = (videoId) => {
  const [info, setInfo] = useState();
  const [isLoading, setLoading] = useState(true);

  useEffect(() => {
    getVideoSubtitles(videoId)
      .then((val) => {
        setInfo(val);
        setLoading(false);
      })
      .catch((e) => {
        setLoading(false);
        throw e;
      });
  }, [videoId]);

  return [info, isLoading];
};

export default useVideoSubtitle;
