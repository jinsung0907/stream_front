import { useEffect, useState } from "react";
import { getVideoInfoById } from "../lib/Video";
import { useUserDispatch } from "./UserContext";

export const useUserLogin = (videoId) => {
  const [info, setInfo] = useState();
  const [isLoading, setLoading] = useState(true);

  const userDispatch = useUserDispatch();

  async function login() {
    try {
      setLoading(true);
      let res = await doLogin(email, password);
      setLoading(false);

      switch (res.status) {
        case "success":
          userDispatch({ type: "LOGIN", data: res.data });

          break;
        case "noemail":
          setErr("이메일을 입력해주세요.");
          break;
        case "nopassword":
          setErr("비밀번호를 입력해주세요.");
          break;
        case "loginfail":
          setErr("로그인에 실패했습니다. 이메일/비밀번호를 확인해주세요.");
          break;
        default:
          setErr("오류 발생.");
      }
    } catch (e) {
      setErr(e);
    }
  }

  useEffect(() => {
    doLogin(videoId)
      .then((val) => {
        setInfo(val);
        setLoading(false);
      })
      .catch((e) => {
        setLoading(false);
        throw e;
      });
  }, []);

  return [info, isLoading];
};

export default useUserLogin;
