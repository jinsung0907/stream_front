import { useEffect, useState } from "react";
import { getVideoInfoById } from "../lib/Video";

const useVideoComment = (videoId) => {
  const [info, setInfo] = useState();
  const [isLoading, setLoading] = useState(true);

  useEffect(() => {
    getVideoInfoById(videoId)
      .then((val) => {
        setInfo(val);
        setLoading(false);
      })
      .catch((e) => {
        setLoading(false);
        throw e;
      });
  }, [videoId]);

  return [info, isLoading];
};

export default useVideoComment;
