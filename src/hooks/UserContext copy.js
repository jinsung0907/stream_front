import React, { useReducer, createContext, useContext, useEffect } from "react";
import Cookie from "js-cookie";
import { doLogin, doLogout, getUserMyInfo } from "../lib/User";
import useAsyncReducer from "./userAsyncReducer";

const initialValue = {
  logined: false,
  userData: null,
};

async function userReducer(state, action) {
  if (action.type === "LOGIN") {
    let res = await doLogin(action.email, action.password).catch((e) => {
      console.error(e);
      throw new Error("Unhandled fetch error");
    });
    if (res.errorMsg) {
      throw new Error(res.errorMsg);
    } else if (res.status === "success") {
      Cookie.set("token", res.token, { expires: 365 });
      return { logined: true, userData: res.data };
    } else throw new Error("Unhandled statuscode error");
  } else if (action.type === "UPDATE") {
    return { logined: true, userData: action.data };
  } else if (action.type === "LOGOUT") {
    let res = await doLogout().catch((e) => {
      console.error(e);
      throw new Error("Unhandled fetch error");
    });
    if (res.errorMsg) {
      throw new Error(res.errorMsg);
    } else if (res.status === "success") {
      Cookie.remove("token");
      return { logined: false, userData: null };
    } else throw new Error("Unhandled statuscode error");
  } else {
    throw new Error(`Unhandled action type: ${action.type}`);
  }
}

const UserStateContext = createContext();
const UserDispatchContext = createContext();

export function UserProvider({ children }) {
  const [state, dispatch] = useAsyncReducer(userReducer, initialValue);

  async function updateUserInfo() {
    // if logined
    let token = Cookie.get("token");

    if (typeof token !== "undefined") {
      let res = await getUserMyInfo();
      if (res.status === "logined") {
        dispatch({ type: "UPDATE", data: res.data });
      }
    }
  }

  useEffect(() => {
    updateUserInfo();
  }, []);

  return (
    <UserStateContext.Provider value={state}>
      <UserDispatchContext.Provider value={dispatch}>
        {children}
      </UserDispatchContext.Provider>
    </UserStateContext.Provider>
  );
}

export function useUserState() {
  const context = useContext(UserStateContext);
  if (!context) {
    throw new Error("Cannot find UserProvider");
  }
  return context;
}

export function useUserDispatch() {
  const context = useContext(UserDispatchContext);
  if (!context) {
    throw new Error("Cannot find UserProvider");
  }
  return context;
}
