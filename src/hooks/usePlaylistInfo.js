import { useEffect, useState } from "react";
import { getPlaylistById } from "../lib/Playlist";

const usePlaylistInfo = (playlistId) => {
  const [info, setInfo] = useState();
  const [isLoading, setLoading] = useState(true);

  useEffect(() => {
    getPlaylistById(playlistId)
      .then((val) => {
        setInfo(val);
        setLoading(false);
      })
      .catch((e) => {
        setLoading(false);
        throw e;
      });
  }, [playlistId]);

  return [info, isLoading];
};

export default usePlaylistInfo;
