import React, { useReducer, createContext, useContext, useEffect } from "react";
import Cookie from "js-cookie";
import { getUserMyInfo } from "../lib/User";

const initialValue = {
  logined: false,
  userData: null,
};

function userReducer(state, action) {
  switch (action.type) {
    case "LOGIN":
      action.token && Cookie.set("token", action.token, { expires: 365 });
      return { logined: true, userData: action.data };
    case "LOGOUT":
      Cookie.remove("token");
      return { logined: false, userData: null };
    default:
      throw new Error(`Unhandled action type: ${action.type}`);
  }
}

const UserStateContext = createContext();
const UserDispatchContext = createContext();

export function UserProvider({ children }) {
  const [state, dispatch] = useReducer(userReducer, initialValue);

  async function updateUserInfo() {
    // if logined
    let token = Cookie.get("token");

    if (typeof token !== "undefined") {
      let res = await getUserMyInfo();
      if (res.status === "logined") {
        dispatch({ type: "LOGIN", data: res.data });
      }
    }
  }

  useEffect(() => {
    updateUserInfo();
  }, []);

  return (
    <UserStateContext.Provider value={state}>
      <UserDispatchContext.Provider value={dispatch}>
        {children}
      </UserDispatchContext.Provider>
    </UserStateContext.Provider>
  );
}

export function useUserState() {
  const context = useContext(UserStateContext);
  if (!context) {
    throw new Error("Cannot find UserProvider");
  }
  return context;
}

export function useUserDispatch() {
  const context = useContext(UserDispatchContext);
  if (!context) {
    throw new Error("Cannot find UserProvider");
  }
  return context;
}
