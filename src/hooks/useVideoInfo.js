import { useEffect, useState } from "react";
import { getVideoInfoById } from "../lib/Video";

const useVideoInfo = (videoId) => {
  const [info, setInfo] = useState();
  const [error, setError] = useState();
  const [isLoading, setLoading] = useState(true);

  useEffect(() => {
    getVideoInfoById(videoId)
      .then((val) => {
        if (val.status === "success") {
          setInfo(val.data);
        } else {
          setError(val.status);
        }

        setLoading(false);
      })
      .catch((e) => {
        setLoading(false);
        console.error(e);
        setError("GENERIC ERROR");
      });
  }, [videoId]);

  return [info, isLoading, error];
};

export default useVideoInfo;
