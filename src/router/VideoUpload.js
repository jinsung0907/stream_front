import {
  Button,
  FormErrorMessage,
  FormLabel,
  FormControl,
  Input,
  Flex,
  ModalBody,
  ModalHeader,
  ModalOverlay,
  ModalContent,
  Modal,
  Progress,
  Select,
  Spinner,
  Textarea,
} from "@chakra-ui/react";
import Cookies from "js-cookie";
import { useState } from "react";
import { FiUploadCloud } from "react-icons/fi";
import { useHistory } from "react-router-dom";
import Navbar from "../components/Navbar/Navbar";
import { APIURL } from "../lib/constants";

const VideoUpload = (props) => {
  const [isUploading, setUploading] = useState(false);
  const [uploadPercent, setUploadPercent] = useState(0);
  const [isSubtitle, setSubtitle] = useState(false);
  const [inputError, setInputError] = useState({});

  let history = useHistory();

  const onClickUpload = (e) => {
    e.preventDefault();
    let formData = new FormData(document.getElementById("uploadForm"));
    let xhr = new XMLHttpRequest();

    // validation 시작
    let isErr = false;
    setInputError({});
    let errorValue = {};
    if (!formData.get("title")) {
      errorValue = { ...errorValue, title: "제목을 입력하세요." };
      isErr = true;
    }
    if (!formData.get("description")) {
      errorValue = { ...errorValue, description: "설명을 입력하세요." };
      isErr = true;
    }
    if (!formData.get("visible")) {
      errorValue = { ...errorValue, visible: "공개설정을 선택하세요." };
      isErr = true;
    }
    if (formData.get("video").name === "") {
      errorValue = { ...errorValue, video: "파일을 선택하세요." };
      isErr = true;
    }
    if (
      formData.get("subtitle").name !== "" &&
      !formData.get("subtitleTitle")
    ) {
      errorValue = {
        ...errorValue,
        subtitleTitle: "자막 제목을 입력해주세요.",
      };
      isErr = true;
    }
    if (isErr) {
      setInputError(errorValue);
      return;
    }
    // validation 끝

    setUploading(true);

    xhr.open("POST", APIURL + "/upload");
    xhr.setRequestHeader("Authorization", "Bearer " + Cookies.get("token"));

    xhr.addEventListener("load", function () {
      let res = JSON.parse(xhr.responseText);
      if (res.status === "success") {
        alert("업로드 완료.\n내부 처리 후 업로드 됩니다.");
      } else if (res.errorMsg) {
        alert(res.errorMsg);
      } else {
        alert("GENERIC ERROR");
      }
      setUploading(false);
      history.push("/studio");
    });

    xhr.upload.addEventListener("progress", function (event) {
      if (event.lengthComputable) {
        var complete = ((event.loaded / event.total) * 100) | 0;
        setUploadPercent(complete);
      }
    });

    xhr.send(formData);
  };

  const onSubtitleChange = (e) => {
    setSubtitle(e.target.value);
  };

  return (
    <>
      <Navbar />
      <Flex p="3" maxW="1300px" justifyContent="center" flexFlow="column">
        <form id="uploadForm" action={`${APIURL}/upload`} method="post">
          <FormControl isRequired isInvalid={inputError.title}>
            <FormLabel>제목</FormLabel>
            <Input name="title" />
            <FormErrorMessage>{inputError.title}</FormErrorMessage>
          </FormControl>

          <FormControl isRequired isInvalid={inputError.description} mt="3">
            <FormLabel>설명</FormLabel>
            <Textarea name="description" />
            <FormErrorMessage>{inputError.description}</FormErrorMessage>
          </FormControl>

          <FormControl isRequired isInvalid={inputError.visible} mt="3">
            <FormLabel>공개 설정</FormLabel>
            <Select name="visible">
              <option value="public">공개</option>
              <option value="unlisted">미공개</option>
              <option value="private">비공개</option>
            </Select>
            <FormErrorMessage>{inputError.visible}</FormErrorMessage>
          </FormControl>

          <FormControl isRequired isInvalid={inputError.video} mt="3">
            <FormLabel>영상 파일</FormLabel>
            <Input name="video" type="file" />
            <FormErrorMessage>{inputError.video}</FormErrorMessage>
          </FormControl>

          <FormControl mt="3">
            <FormLabel>자막 파일(선택)</FormLabel>
            <Input name="subtitle" onChange={onSubtitleChange} type="file" />
          </FormControl>

          {isSubtitle && (
            <FormControl isRequired isInvalid={inputError.subtitleTitle} mt="3">
              <FormLabel>자막 제목</FormLabel>
              <Input name="subtitleTitle" />
              <FormErrorMessage>{inputError.subtitleTitle}</FormErrorMessage>
            </FormControl>
          )}

          <Button
            onClick={onClickUpload}
            leftIcon={<FiUploadCloud />}
            colorScheme="teal"
            type="submit"
            mt="4"
            isLoading={isUploading}
          >
            업로드
          </Button>
        </form>
      </Flex>
      <Modal isOpen={isUploading}>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader textAlign="center">업로드중...</ModalHeader>
          <ModalBody textAlign="center">
            <Spinner mb="2" />
            <Progress value={uploadPercent} mb="3" />
          </ModalBody>
        </ModalContent>
      </Modal>
    </>
  );
};

export default VideoUpload;
