import { Spinner, SimpleGrid } from "@chakra-ui/react";
import { useEffect, useState } from "react";
import { useParams } from "react-router";
import Navbar from "../components/Navbar/Navbar";
import VideoListItem from "../components/Video/VideoListItem";
import { doSearch } from "../lib/Search";

const Search = () => {
  const [isLoading, setLoading] = useState(true);
  const [list, setList] = useState([]);
  const { str } = useParams();

  useEffect(() => {
    async function getList() {
      let res = await doSearch(str);

      if (res.status === "success") {
        setList(res.data);
        setLoading(false);
      } else {
        setList([]);
        setLoading(false);
      }
    }

    getList();
  }, [str]);

  return (
    <>
      <Navbar searchstr={str} />
      <SimpleGrid
        w="100%"
        h="100%"
        maxW="1300px"
        minChildWidth="240px"
        gap="30px"
        px="3"
        justifyContent="space-between"
        alignItems="center"
      >
        {isLoading ? (
          <Spinner />
        ) : (
          list.map((val) => <VideoListItem info={val} />)
        )}
      </SimpleGrid>
    </>
  );
};

export default Search;
