import VideoList from "../components/Video/VideoList";
import useVideoInfo from "../hooks/useVideoInfo";

const Home = () => {
  const [info, isLoading] = useVideoInfo("n4B92o");

  return <div>{isLoading ? "loading" : <VideoList items={[info]} />}</div>;
};

export default Home;
