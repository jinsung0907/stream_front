import {
  useDisclosure,
  Image,
  Box,
  Flex,
  Text,
  VStack,
  Spinner,
  useToast,
  Container,
} from "@chakra-ui/react";

import { useEffect, useState } from "react";
import StudioVideoModify from "../components/Modal/StudioVideoModify";
import Navbar from "../components/Navbar/Navbar";
import StudioVideoItem from "../components/Studio/StudioVideoItem";
import { CDNURL } from "../lib/constants";
import { getMyVideoList } from "../lib/Studio";
import { visibleToString } from "../lib/Video";
const Studio = () => {
  const [isLoading, setLoading] = useState(true);
  const [videoList, setVideoList] = useState([]);
  const [currentInfo, setCurrentInfo] = useState();
  const { isOpen, onOpen, onClose } = useDisclosure();
  const toast = useToast();

  const openModal = (val) => {
    setCurrentInfo(val);
    onOpen();
  };

  const onModified = async () => {
    toast({
      title: "저장 완료.",
      description: "수정사항이 저장되었습니다.",
      status: "success",
      duration: 5000,
    });
    await getVideoList();
  };

  async function getVideoList() {
    setLoading(true);
    let res = await getMyVideoList();

    if (res.status === "success") {
      setVideoList(res.data);
      setLoading(false);
    } else {
      alert(`오류 발생(${res.status}) : ${res.errorMsg}`);
    }
  }

  useEffect(() => {
    async function _getVideoList() {
      getVideoList();
    }

    _getVideoList();
  }, []);

  if (isLoading === true) {
    return (
      <>
        <Navbar />
        <VStack>
          <Spinner />
        </VStack>
      </>
    );
  } else {
    return (
      <>
        <Navbar />
        <VStack w="100%" maxW="1300px" m="auto">
          {videoList.map((val) => (
            <StudioVideoItem key={val.id} item={val} onClick={openModal} />
          ))}
        </VStack>
        {isOpen && (
          <StudioVideoModify
            info={currentInfo}
            isOpen={isOpen}
            onOpen={onOpen}
            onClose={onClose}
            onModified={onModified}
          />
        )}
      </>
    );
  }
};

export default Studio;
