import { Tabs, Tab, TabList, TabPanel, TabPanels } from "@chakra-ui/react";
import Navbar from "../components/Navbar/Navbar";
import PlaylistList from "../components/PlaylistList";
import VideoList from "../components/VideoList";

const Home = () => {
  return (
    <>
      <Navbar />
      <Tabs variant="line" isLazy>
        <TabList>
          <Tab>영상</Tab>
          <Tab>재생목록</Tab>
        </TabList>
        <TabPanels>
          <TabPanel>
            <VideoList />
          </TabPanel>
          <TabPanel>
            <PlaylistList />
          </TabPanel>
        </TabPanels>
      </Tabs>
    </>
  );
};

export default Home;
