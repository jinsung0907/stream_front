import {
  Button,
  Icon,
  Box,
  Flex,
  Divider,
  Text,
  Heading,
  useDisclosure,
  useToast,
  Spinner,
} from "@chakra-ui/react";
import { useParams } from "react-router";
import Navbar from "../components/Navbar/Navbar";
import PlayerArea from "../components/watch/PlayerArea";
import useVideoInfo from "../hooks/useVideoInfo";
import useVideoSubtitle from "../hooks/useVideoSubtitle";
import { FiThumbsUp, FiThumbsDown, FiList } from "react-icons/fi";
import VideoComments from "../components/VideoComment/VideoComments";
import { useEffect, useState } from "react";
import { videoDislike, videoLike, videoViewCount } from "../lib/Video";
import dayjs from "dayjs";
import { useUserState } from "../hooks/UserContext";
import VideoSubtitleList from "../components/watch/VideoSubtitleList";
import PlaylistAddDrawer from "../components/Drawer/PlaylistAddDrawer";
import { useLocation } from "react-router-dom";
import { getPlaylistById, reOrderPlaylist } from "../lib/Playlist";
import PlaylistArea from "../components/watch/PlaylistArea";
import { handleFetchErrorWithToast } from "../lib/common";

const Home = () => {
  let { id } = useParams();
  let playlistId = new URLSearchParams(useLocation().search).get("playlistId");

  const [info, isInfoLoading, vidErr] = useVideoInfo(id);
  const [playlist, setPlaylist] = useState();
  const [playlistLoading, setPlaylistLoading] = useState(false);
  const [subtitle, setSubtitle] = useState();
  const [subtitles, isSubLoading] = useVideoSubtitle(id);
  const [likeBtn, setLikeBtn] = useState(0);
  const [dislikeBtn, setDislikeBtn] = useState(0);
  const [isLikeBtnLoading, setLikeBtnLoading] = useState(false);

  const playlistDrawer = useDisclosure();
  const toast = useToast();

  const user = useUserState();

  const onClickLikeBtn = async (type) => {
    if (!user.logined) {
      toast({
        title: "로그인이 필요합니다.",
        status: "error",
        duration: 2000,
      });
      return;
    }
    setLikeBtnLoading(true);
    let res;
    if (type) {
      res = await videoLike(info.id);
    } else {
      res = await videoDislike(info.id);
    }

    if (res.status === "success") {
      setLikeBtn(likeBtn + res.data.like);
      setDislikeBtn(dislikeBtn + res.data.dislike);
    } else if (res.status === "requireLogin") {
      toast({
        title: "로그인이 필요합니다.",
        status: "error",
        duration: 2000,
      });
    }
    setLikeBtnLoading(false);
  };

  //플레이리스트 페이지
  useEffect(() => {
    async function getPlaylist() {
      setPlaylistLoading(true);
      let play = await getPlaylistById(playlistId);
      setPlaylist(play);
      setPlaylistLoading(false);
    }

    if (playlistId) {
      getPlaylist();
    }
  }, [playlistId]);

  useEffect(() => {
    if (!isInfoLoading && !vidErr) {
      setLikeBtn(info.likeCount);
      setDislikeBtn(info.dislikeCount);
      videoViewCount(info.id);
    }
  }, [isInfoLoading, vidErr, info]);

  const onClickSubtitleItem = (id) => {
    setSubtitle(subtitles[id]);
  };

  const onClickAddPlaylistButton = (e) => {
    if (!user.logined) {
      toast({
        title: "로그인이 필요합니다.",
        status: "error",
        duration: 3000,
      });
      return;
    }
    playlistDrawer.onOpen();
  };

  const onPlaylistDragEnd = async (from, to) => {
    let res = await reOrderPlaylist(playlist.id, playlist.items[from].id, to);

    if (res.status === "success") {
      toast({
        title: "순서변경 완료",
        status: "success",
        ducation: 1500,
      });
      orderPlaylistLocal(from, to);
    } else {
      handleFetchErrorWithToast(res, toast);
    }
  };

  const orderPlaylistLocal = (from, to) => {
    let newitems = [...playlist.items];
    const item = newitems.splice(from, 1)[0];
    newitems.splice(to, 0, item);

    let newplaylist = { ...playlist };
    newplaylist.items = newitems;

    setPlaylist(newplaylist);
  };

  return (
    <>
      <Navbar />
      <Flex p="3" maxW="1300px" justifyContent="center" flexFlow="column">
        {isInfoLoading ? null : vidErr ? (
          vidErr
        ) : (
          <>
            <Flex flexWrap="wrap">
              <Box flexGrow="1">
                <PlayerArea
                  info={info}
                  subtitle={subtitle}
                  //subtitles={subtitles}
                  isInfoLoading={isInfoLoading}
                  isSubLoading={isSubLoading}
                  showSubtitleOption
                />
              </Box>
              {playlist && (
                <PlaylistArea
                  playlist={playlist}
                  playingVideoInfo={info}
                  onDragEnd={onPlaylistDragEnd}
                />
              )}
            </Flex>

            <Divider my="3" />
            <Box>
              <Heading size="md" fontWeight="bold">
                {info.title}
              </Heading>
              <Flex
                justifyContent="space-between"
                alignItems="flex-end"
                flexWrap="wrap"
              >
                <Text size="sm">
                  조회수 {info.viewCount}회 -{" "}
                  {dayjs.unix(info.publicDate).format("YYYY. MM. DD.")}
                </Text>
                <Box mt="1">
                  <Button
                    mr="2"
                    variant="ghost"
                    colorScheme="black"
                    onClick={onClickAddPlaylistButton}
                  >
                    <Icon mr="2" as={FiList} /> 재생목록
                  </Button>

                  <Button
                    isLoading={isLikeBtnLoading}
                    mr="2"
                    variant="ghost"
                    colorScheme="blue"
                    onClick={() => onClickLikeBtn(true)}
                  >
                    <Icon mr="2" as={FiThumbsUp} /> 좋아요 {likeBtn}
                  </Button>
                  <Button
                    isLoading={isLikeBtnLoading}
                    variant="ghost"
                    colorScheme="red"
                    onClick={() => onClickLikeBtn(false)}
                  >
                    <Icon mr="2" as={FiThumbsDown} />
                    싫어요 {dislikeBtn}
                  </Button>
                </Box>
              </Flex>
            </Box>
            <Divider my="2" />
            <Box>{info.description}</Box>
            <Divider my="2" />
            {isSubLoading ? (
              <Spinner />
            ) : (
              <VideoSubtitleList
                info={info}
                list={subtitles}
                selected={subtitle}
                onClick={onClickSubtitleItem}
              />
            )}
            <Divider my="2" />
            <VideoComments videoInfo={info} />
            <Divider my="2" />
          </>
        )}
      </Flex>
      {playlistDrawer.isOpen && (
        <PlaylistAddDrawer
          isOpen={playlistDrawer.isOpen}
          onOpen={playlistDrawer.onOpen}
          onClose={playlistDrawer.onClose}
          videoInfo={info}
          userInfo={user}
        />
      )}
    </>
  );
};

export default Home;
