import "./App.css";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Home from "./router/Home";
import Test from "./router/Test";
import Watch from "./router/Watch";
import VideoUpload from "./router/VideoUpload";
import Search from "./router/Search";
import Studio from "./router/Studio";

function App() {
  return (
    <Router>
      <Switch>
        <Route exact path="/">
          <Home />
        </Route>
        <Route exact path="/watch/:id">
          <Watch />
        </Route>
        <Route exact path="/search">
          <Search />
        </Route>
        <Route exact path="/search/:str">
          <Search />
        </Route>
        <Route exact path="/upload">
          <VideoUpload />
        </Route>
        <Route exact path="/studio">
          <Studio />
        </Route>
        <Route exact path="/test">
          <Test />
        </Route>
      </Switch>
    </Router>
  );
}

export default App;
