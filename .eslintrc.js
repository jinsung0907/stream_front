module.exports = {
  env: {
    commonjs: true,
    es6: true,
  },
  extends: [
    //"standard",
    "react-app",
    "plugin:prettier/recommended",
    "plugin:jsdoc/recommended",
  ],
  plugins: ["react"],
  globals: {
    Atomics: "readonly",
    SharedArrayBuffer: "readonly",
  },
  parserOptions: {
    ecmaVersion: 2018,
  },
  rules: {
    "prettier/prettier": [
      "error",
      {
        endOfLine: "auto",
      },
    ],
    "jsdoc/require-jsdoc": 0,
  },
};
